<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class PostsController
{
    /**
     * @Route("/posts", name="posts")
     */
    public function index(Environment $twig, PostRepository $postRepository)
    {
        $posts = $postRepository->findAll();

        return new Response($twig->render('posts/index.html.twig', compact('posts')));
    }

    /**
     * @Route("/posts/new", name ="posts_new")
     */
    public function new(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $form = $this->treatForm($doctrine, $request, $formFactory);

        return new Response($twig->render('posts/form.html.twig', [
            'form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/posts/{id}/edit", name ="posts_edit")
     */
    public function edit($id, Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $form = $this->treatForm($doctrine, $request, $formFactory, $id);

        return new Response($twig->render('posts/form.html.twig', [
            'form'  => $form->createView(),
        ]));
    }

    /**
     * @param RegistryInterface $doctrine
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param int|null $id
     * @return \Symfony\Component\Form\FormInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function treatForm(RegistryInterface $doctrine, Request $request, FormFactoryInterface $formFactory, int $id = null)
    {
        $posts = $doctrine->getRepository(Post::class)->findAll();
        $form = $formFactory->createBuilder(PostType::class, !is_null($id) ? $posts[$id] : null)->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine->getEntityManager()->flush();
        }

        return $form;
    }
}
