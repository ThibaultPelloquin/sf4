# README #

Test SF4

## Notes de développement ##

Installation de Symfony 4 via composer :

``composer create-project symfony/skeleton sf4``

*(Optionnel)* Uniquement pour les utilisateurs de Docker : Mise en place de l'Apache Pack

Sans ça, les routes ne fonctionnent pas (Erreur 404).

``composer require symfony/apache-pack``

Mise en place de Twig :

``composer require twig``

Mise en place des Annotations : 

``composer require annotations``

Mise en place du Profiler (barre de débug)

``composer require profiler``

Mise en place du Maker (script de génération de fichiers Symfony)

``composer require maker-bundle``

Mise en place des Form *et* Validator

``composer require form validator``

Mise en place de ORM (Doctrine)

``composer require orm``

On remplit le fichier .env comme suit (pour Docker)

``DATABASE_URL=mysql://root:root@mysql:3306/sf4``

On crée la Base de Données via Doctrine (vide) :

``php bin/console doctrine:database:create sf4``

On génère la première entité puis on la remplit (champs + ORM\Column).

Création de la Migration (Doctrine)

``php bin/console doctrine:migrations:diff``

Un fichier src/Migrations/Version[YmdHis].php est généré.
Celui-ci contient les commandes SQL qui seront lancées lors de la migration.

Lancement de la Migration (Doctrine)

``php bin/console doctrine:migrations:migrate``

Le script lance les commandes SQL (création/remplissage des tables).
Lors de la première migration, une table `migration_versions` est créée.
Cette table contient la liste des versions qui ont déjà été lancées sur l'environnement.

*(Optionel)* Pour les utilisateurs de PhpStorm et de Docker

Pour connecter PhpStorm à la base de données, cliquer sur l'onglet "Database" (à droite de la fenêtre).
Cliquer sur le bouton "Data Source Properties" puis entrer l'URL :

``jdbc:mysql://127.0.0.1:3306/sf4``

Cliquer sur le bouton [Test Connection].

(voir plus bas pour utilisation des Bases de Données dans PhpStorm)

Gestion des formulaires (voir plus bas)


## Tips ##

Connaître la liste des classes et interfaces utilisables en autowiring

``php bin/console debug:autowiring``

Exemple d'utilisation du Maker (en console)

 * Génération d'un contrôleur : ``php bin/console make:controller Posts``
 * Génération d'un formulaire : ``php bin/console make:form Post``
 * Génération d'une entité : ``php bin/console make:entity Post``

## Utilisation des bases de données dans PhpStorm ##

Pour gérer des données en base directement depuis PhpStorm, ouvrir l'onglet "Database".
L'arborescence des bases de données s'affiche.
Double-cliquer sur la table que l'on veut gérer.

 * Pour synchroniser les données, cliquer sur le bouton représentant une double flèche bleue ("Reload Page").
 * Pour insérer des données en base, cliquer sur l'icone représentant une flèche verte avec DB ("Submit").

## Gestion des formulaires ##

Dans le PostsController, création de deux méthodes, correspondant à deux routes
 * new : Nouvelle entrée
 * edit : Edition d'une donnée existante

Une seule vue Twig comportant le formulaire (le même pour new et edit) : templates/posts/form.html.twig

Dans le fichier src/Form/PostType, on définit les types de champs de formulaire et options pour chaque élément du formulaire.

Dans le PostsController, on récupère le formulaire et la requête puis on teste si le formulaire est soumis et valide.
Si c'est le cas on l'enregistre en base via Doctrine.

Les contraintes de validation des champs sont gérées directement dans l'entité à l'aide d'annotations @Assert.
Celles-ci correspondent à Symfony\Component\Validator\Constraints.
